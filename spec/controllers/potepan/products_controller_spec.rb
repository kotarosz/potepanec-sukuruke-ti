require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "Get #show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "returns 200 response" do
      expect(response).to have_http_status :ok
    end

    it "show page" do
      expect(response).to render_template :show
    end

    it "assigns right @product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns right within 4 kind of @related_products" do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
