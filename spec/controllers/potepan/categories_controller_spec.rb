require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:category) { create(:taxonomy, name: "category") }
    let!(:shirt) { create(:taxon, name: "shirt", taxonomy: category) }
    let!(:shirt_product) { create(:product, taxons: [shirt]) }

    before do
      get :show, params: { id: shirt.id }
    end

    it "return succesfully" do
      expect(response).to be_successful
    end

    it "return 200 response" do
      expect(response).to have_http_status "200"
    end

    it "render show page" do
      expect(response).to render_template :show
    end

    it "assigns right @taxonomies" do
      expect(assigns(:taxonomies)).to include category
    end

    it "assigns right @taxon" do
      expect(assigns(:taxon)).to eq shirt
    end

    it "assigns right @products" do
      expect(assigns(:products)).to include shirt_product
    end
  end
end
