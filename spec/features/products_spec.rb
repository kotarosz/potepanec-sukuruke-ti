require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:category) { create(:taxonomy, name: "category") }
  given!(:brand) { create(:taxonomy, name: "brand") }
  given!(:mug) { create(:taxon, name: "mug", parent: category.root, taxonomy: category) }
  given!(:ruby) { create(:taxon, name: "ruby", parent: category.root, taxonomy: brand) }
  given!(:shirts) { create(:taxon, name: "shirts", parent: category.root, taxonomy: category) }
  given!(:ruby_mug) { create(:product, taxons: [mug, ruby]) }
  given!(:other_ruby_mug) { create(:product, taxons: [mug, ruby]) }
  given!(:ruby_shirts) { create(:product, taxons: [shirts, ruby]) }
  given!(:apache) { create(:taxon, name: "apache", parent: brand.root, taxonomy: brand) }
  given!(:apache_shirts) { create(:product, taxons: [shirts, apache]) }

  background do
    visit potepan_product_path(id: ruby_mug.id)
  end

  scenario "has right main product " do
    expect(page).to have_title "#{ruby_mug.name} - BIGBAG Store"
    expect(page).to have_content ruby_mug.name
    expect(page).to have_content ruby_mug.display_price
    expect(page).to have_content ruby_mug.description
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(mug.id)
  end

  scenario "has right related product " do
    within(".productsContent") do
      expect(page).to have_content other_ruby_mug.name
      expect(page).to have_content ruby_shirts.name
      expect(page).not_to have_content ruby_mug.name
      expect(page).not_to have_content apache_shirts.name
      click_on other_ruby_mug.name
      expect(current_path).to eq potepan_product_path(other_ruby_mug.id)
    end
  end
end
