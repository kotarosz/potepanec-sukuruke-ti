require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:category) { create(:taxonomy, name: "category") }
  given!(:shirt) { create(:taxon, name: "shirt", parent: category.root, taxonomy: category) }
  given!(:shirt_product) { create(:product, taxons: [shirt]) }

  given!(:brand) { create(:taxonomy, name: "brand") }
  given!(:rails) { create(:taxon, name: "rails", parent: brand.root, taxonomy: brand) }
  given!(:rails_product) { create(:product, taxons: [rails]) }

  background do
    visit potepan_category_path(shirt.id)
  end

  scenario "has correct path and title " do
    expect(page).to have_current_path potepan_category_path(shirt.id)
    expect(page).to have_title "#{shirt.name} - BIGBAG Store"
  end

  scenario "show correct taxon" do
    within(".sideBar") do
      expect(page).to have_content category.name
      expect(page).to have_link shirt.name
      expect(page).to have_content brand.name
      expect(page).to have_link rails.name
      expect(page).not_to have_content shirt_product.name
      expect(page).to have_selector ".navbar-side-collapse", text: shirt.name
      expect(page).to have_selector ".priceRange", text: "値段から探す"
      click_on rails.name
      expect(current_path).to eq potepan_category_path(rails.id)
    end
  end

  scenario "show correct product " do
    within(".productBox") do
      expect(page).to have_content shirt_product.name
      expect(page).to have_content shirt_product.display_price
      expect(page).not_to have_content rails_product
      expect(page).to have_selector "h5", text: shirt_product.name
      expect(page).to have_selector "h3", text: shirt_product.display_price
      click_on shirt_product.name
      expect(current_path).to eq potepan_product_path(shirt_product.id)
      click_on shirt_product.display_price
      expect(current_path).to eq potepan_product_path(shirt_product.id)
    end
  end
end
