require 'rails_helper'

RSpec.describe Spree::Product, type: :decorator do
  let!(:mug) { create(:taxon, name: "mug") }
  let!(:ruby) { create(:taxon, name: "ruby") }
  let!(:shirts) { create(:taxon, name: "shirts") }
  let!(:apache) { create(:taxon, name: "apache") }
  let!(:product) { create(:product, taxons: [mug, ruby]) }
  let!(:related_product) { create(:product, taxons: [mug, ruby]) }
  let!(:non_related_product) { create(:product, taxons: [shirts, apache]) }
  let!(:number) { 1 }

  it "returns right related products" do
    expect(Spree::Product.related_products(product, number)).to include related_product
  end

  it " no returns non related products" do
    expect(Spree::Product.related_products(product, number)).not_to include non_related_product
  end
end
