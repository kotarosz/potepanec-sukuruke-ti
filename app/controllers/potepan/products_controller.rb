class Potepan::ProductsController < ApplicationController
  MAX_LIMITED_NUMBER_PRODUCT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product, MAX_LIMITED_NUMBER_PRODUCT).random
  end
end
