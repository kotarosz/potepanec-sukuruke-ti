Spree::Product.class_eval do
  scope :random, -> { order(Arel.sql("RAND()")) }
  scope :including_images_price, -> { includes(master: [:images, :default_price]) }
  scope :related_products, ->(product, number) {
    in_taxons(product.taxons).
      where.not(id: product.id).
      distinct.
      including_images_price.
      limit(number)
  }
end
